﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Android;
using Xamarin.UITest.Queries;

namespace UITestMoney3
{
    [TestFixture]
    public class LoginTests
    {
        AndroidApp app;

        [SetUp]
        public void BeforeEachTest()
        {
            app = ConfigureApp
                .Android
                .ApkFile("C:/Users/keilo/source/repos/UITestMoney3/money.apk")
                .StartApp();
        }

        //1	Prueba de asignación de código de acceso.
        [Test]
        public void LoginTest1()
        {
            //Arrange

            //Act
            app.Tap(b => b.Marked("cerrar"));
            app.Tap(b => b.Marked("Config."));
            app.Tap(b => b.Marked("Contraseña"));
            app.Tap(b => b.Marked("pad1"));
            app.Tap(b => b.Marked("pad2"));
            app.Tap(b => b.Marked("pad3"));
            app.Tap(b => b.Marked("pad4"));
            app.Tap(b => b.Marked("pad1"));
            app.Tap(b => b.Marked("pad2"));
            app.Tap(b => b.Marked("pad3"));
            app.Tap(b => b.Marked("pad4"));
            app.Tap(b => b.Marked("Trans."));


            //Assert [AppCompatTextView] text: "No hay datos."
            AppResult[] results = app.WaitForElement(c => c.Marked("noDataImg"));
            Assert.IsTrue(results.Any());
        }

        //2. Prueba de ingreso de código pre ingresado.
        [Test]
        public void LoginTest2()
        {
            //Arrange

            //Act
            app.Tap(b => b.Marked("cerrar"));
            app.Tap(b => b.Marked("Config."));
            app.Tap(b => b.Marked("Contraseña"));
            app.Tap(b => b.Marked("pad1"));
            app.Tap(b => b.Marked("pad2"));
            app.Tap(b => b.Marked("pad3"));
            app.Tap(b => b.Marked("pad4"));
            app.Tap(b => b.Marked("pad1"));
            app.Tap(b => b.Marked("pad2"));
            app.Tap(b => b.Marked("pad3"));
            app.Tap(b => b.Marked("pad4"));
            app.Tap(b => b.Marked("Contraseña"));
            app.Tap(b => b.Marked("pad1"));
            app.Tap(b => b.Marked("pad2"));
            app.Tap(b => b.Marked("pad3"));
            app.Tap(b => b.Marked("pad4"));
            app.Tap(b => b.Marked("Trans."));


            //Assert [AppCompatTextView] text: "No hay datos."
            AppResult[] results = app.WaitForElement(c => c.Marked("noDataImg"));
            Assert.IsTrue(results.Any());
        }

        //3. Prueba de asignación de código con error y reintento
        [Test]
        public void LoginTest3()
        {
            //Arrange

            //Act
            app.Tap(b => b.Marked("cerrar"));
            app.Tap(b => b.Marked("Config."));
            app.Tap(b => b.Marked("Contraseña"));
            app.Tap(b => b.Marked("pad1"));
            app.Tap(b => b.Marked("pad2"));
            app.Tap(b => b.Marked("pad3"));
            app.Tap(b => b.Marked("pad4"));
            app.Tap(b => b.Marked("pad1"));
            app.Tap(b => b.Marked("pad2"));
            app.Tap(b => b.Marked("pad3"));
            app.Tap(b => b.Marked("pad4"));
            app.Tap(b => b.Marked("Contraseña"));
            app.Tap(b => b.Marked("pad1"));
            app.Tap(b => b.Marked("pad3"));
            app.Tap(b => b.Marked("pad3"));
            app.Tap(b => b.Marked("pad4"));


            //Assert [AppCompatTextView] text: "No hay datos."
            AppResult[] results = app.WaitForElement(c => c.Marked("message").Text("Codigo de acceso incorrecto"));
            Assert.IsTrue(results.Any());
        }
    }
}
