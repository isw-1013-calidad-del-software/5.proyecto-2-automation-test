﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Android;
using Xamarin.UITest.Queries;

namespace UITestMoney3
{
    [TestFixture]
    public class UpdateTests
    {
        AndroidApp app;

        [SetUp]
        public void BeforeEachTest()
        {
            //Inicialización del APK
            app = ConfigureApp
                .Android
                .ApkFile("C:/Users/keilo/source/repos/UITestMoney3/money.apk")
                .StartApp();
        }

        //Prueba de update de gastos, inicialmente se hace un insert con valor erroneo para luego editarlo.
        [Test]
        public void UpdateTest1()
        {
            //Arrange
            string note = "UpdateTest1";
            string description = "cena de Keilor";

            //Act
            //Registro para editar
            app.Tap(b => b.Marked("cerrar"));
            app.Tap(b => b.Marked("addBtnFrame"));
            app.Tap(b => b.Marked("Efectivo"));
            app.Tap(b => b.Marked("Comida"));
            app.Tap(b => b.Marked("amountBlock"));
            app.Tap(b => b.Marked("pad3"));
            app.Tap(b => b.Marked("pad2"));
            app.Tap(b => b.Marked("pad5"));
            app.Tap(b => b.Marked("pad0"));
            app.Tap(b => b.Marked("memoBlock"));
            app.EnterText("eeeeeeeeeeeeeeeeeeeeeeeee");
            app.Tap(b => b.Marked("description"));
            app.EnterText(description);
            app.ScrollDown();
            app.Tap(b => b.Marked("Guardar"));

            //Editar registro
            app.Tap(b => b.Marked("listHeadLayout"));
            app.Tap(b => b.Marked("account"));
            app.Tap(b => b.Marked("Tarjetas de credito"));
            app.ScrollDown();
            app.ClearText(b => b.Marked("memoBlock"));
            app.Tap(b => b.Marked("memoBlock"));
            app.EnterText(note);
            app.Tap(b => b.Marked("Guardar"));

            //Assert
            AppResult[] results = app.WaitForElement(c => c.Marked("titleText").Text(note));
            Assert.IsTrue(results.Any());
        }

        //Prueba de update de ingresos, primero se hace un insert con datos erroneo para luego editarlo 
        [Test]
        public void UpdateTest2()
        {
            //Arrange
            string description = "Reparacion de equipo";
            string note = "UpdateTest2";

            //Act
            app.Tap(b => b.Marked("cerrar"));
            app.Tap(b => b.Marked("addBtnFrame"));
            app.Tap(b => b.Marked("Ingresos"));
            app.Tap(b => b.Marked("Efectivo"));
            app.Tap(b => b.Marked("Salario"));
            app.Tap(b => b.Marked("memoBlock"));
            app.EnterText("dato erroneo");
            app.Tap(b => b.Marked("description"));
            app.EnterText(description);
            app.ScrollDown();
            app.Tap(b => b.Marked("amountBlock"));
            app.Tap(b => b.Marked("pad1"));
            app.Tap(b => b.Marked("pad5"));
            app.Tap(b => b.Marked("pad0"));
            app.Tap(b => b.Marked("pad0"));
            app.Tap(b => b.Marked("pad0"));
            app.ScrollDown();
            app.Tap(b => b.Marked("Guardar"));

            //Editar registro
            app.Tap(b => b.Marked("listHeadLayout"));
            app.Tap(b => b.Marked("account"));
            app.Tap(b => b.Marked("Cuentas"));
            app.Tap(b => b.Marked("category"));
            app.Tap(b => b.Marked("Dinero Extra"));
            app.ScrollDown();
            app.ClearText(b => b.Marked("memoBlock"));
            //app.Tap(b => b.Marked("memoBlock"));
            app.EnterText(note);
            app.Tap(b => b.Marked("amountBlock"));
            app.Tap(b => b.Marked("Guardar"));

            //Assert
            AppResult[] results = app.WaitForElement(c => c.Marked("titleText").Text(note));
            Assert.IsTrue(results.Any());
        }

        //Prueba de update de gastos/ingresos, inicialmente se hace un insert de gastos, luego se edita a ingreso, pero 
        //queda campo vació, por lo que luego se completa y guarda el dato.
        [Test]
        public void UpdateTest3()
        {
            //Arrange
            string note = "UpdateTest3";
            string description = "cena de Keilor";

            //Act
            //Registro para editar
            app.Tap(b => b.Marked("cerrar"));
            app.Tap(b => b.Marked("addBtnFrame"));
            app.Tap(b => b.Marked("Efectivo"));
            app.Tap(b => b.Marked("Comida"));
            app.Tap(b => b.Marked("amountBlock"));
            app.Tap(b => b.Marked("pad3"));
            app.Tap(b => b.Marked("pad2"));
            app.Tap(b => b.Marked("pad5"));
            app.Tap(b => b.Marked("pad0"));
            app.Tap(b => b.Marked("memoBlock"));
            app.EnterText("dato erroneo");
            app.Tap(b => b.Marked("description"));
            app.EnterText(description);
            app.ScrollDown();
            app.Tap(b => b.Marked("Guardar"));

            //Editar registro con campo en blanco
            app.Tap(b => b.Marked("listHeadLayout"));
            app.Tap(b => b.Marked("Ingresos"));
            app.ScrollDown();
            app.Tap(b => b.Marked("Guardar"));

            //Datos faltantes
            app.Tap(b => b.Marked("Mesada"));
            app.ClearText(b => b.Marked("memoBlock"));
            app.EnterText(note);
            app.Tap(b => b.Marked("amountBlock"));
            app.Tap(b => b.Marked("Guardar"));

            //Assert
            AppResult[] results = app.WaitForElement(c => c.Marked("titleText").Text(note));
            Assert.IsTrue(results.Any());
        }

    }
}
