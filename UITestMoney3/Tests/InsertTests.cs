﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Android;
using Xamarin.UITest.Queries;

namespace UITestMoney3
{
    [TestFixture]
    public class InsertTests
    {
        AndroidApp app;

        [SetUp]
        public void BeforeEachTest()
        {
            //Inicialización del APK
            app = ConfigureApp
                .Android
                .ApkFile("C:/Users/keilo/source/repos/UITestMoney3/money.apk")
                .StartApp();
        }

        //Clase para mapear elementos
        [Test]
        public void Repl()
        {
            app.Repl();
        }

        //1. Prueba de insertar un gasto exitosamente.
        [Test]
        public void InsertTest1()
        {
            //Arrange
            string note = "InsertTest1";
            string description = "Medicamentos de Keilor";

            //Act
            app.Tap(b => b.Marked("cerrar"));
            app.Tap(b => b.Marked("addBtnFrame"));
            app.Tap(b => b.Marked("Efectivo"));
            app.Tap(b => b.Marked("Salud"));
            app.Tap(b => b.Marked("amountBlock"));
            app.Tap(b => b.Marked("pad6"));
            app.Tap(b => b.Marked("pad5"));
            app.Tap(b => b.Marked("pad0"));
            app.Tap(b => b.Marked("pad0"));
            app.Tap(b => b.Marked("memoBlock"));
            app.EnterText(note);
            app.Tap(b => b.Marked("description"));
            app.EnterText(description);
            app.Tap(b => b.Marked("amountBlock"));
            app.Tap(b => b.Marked("Guardar"));

            //Assert
            AppResult[] results = app.WaitForElement(c => c.Marked("titleText").Text(note));
            Assert.IsTrue(results.Any());
        }

        //2. Prueba de insertar un ingreso exitosamente.
        [Test]
        public void InsertTest2()
        {
            //Arrange
            string description = "Salario mes Agosto";
            string note = "InsertTest2";

            //Act
            app.Tap(b => b.Marked("cerrar"));
            app.Tap(b => b.Marked("addBtnFrame"));
            app.Tap(b => b.Marked("Ingresos"));
            app.Tap(b => b.Marked("Efectivo"));
            app.Tap(b => b.Marked("Salario"));
            app.Tap(b => b.Marked("amountBlock"));
            app.Tap(b => b.Marked("pad1"));
            app.Tap(b => b.Marked("pad5"));
            app.Tap(b => b.Marked("pad3"));
            app.Tap(b => b.Marked("pad0"));
            app.Tap(b => b.Marked("pad7"));
            app.Tap(b => b.Marked("pad0"));
            app.Tap(b => b.Marked("pad0"));
            app.Tap(b => b.Marked("memoBlock"));
            app.EnterText(note);
            app.Tap(b => b.Marked("description"));
            app.EnterText(description);
            app.ScrollDown();
            app.Tap(b => b.Marked("Guardar"));

            //Assert
            AppResult[] results = app.WaitForElement(c => c.Marked("titleText").Text(note));
            Assert.IsTrue(results.Any());
        }

        //3. Prueba de insertar gastos con datos faltantes y error, finalmente corrección de estos y guardado de registro exitosamente.
        [Test]
        public void InsertTest3()
        {
            //Arrange
            string description = "Prueba fallida";
            string note = "InsertTest3";
            
            //Act
            //Verificación de no guardar en caso 
            app.Tap(b => b.Marked("cerrar"));
            app.Tap(b => b.Marked("addBtnFrame"));
            app.Tap(b => b.Marked("Efectivo"));
            app.Tap(b => b.Marked("amountBlock"));
            app.Tap(b => b.Marked("memoBlock"));
            app.EnterText("valor falso");
            app.Tap(b => b.Marked("description"));
            app.EnterText(description);
            app.ScrollDown();
            app.Tap(b => b.Marked("Guardar"));

            //Datos faltantes
            app.Tap(b => b.Marked("Transporte"));
            app.ScrollDown();
            app.ClearText(b => b.Marked("memoBlock"));
            app.Tap(b => b.Marked("memoBlock"));
            app.EnterText(note);
            app.Tap(b => b.Marked("amountBlock"));
            app.Tap(b => b.Marked("pad9"));
            app.Tap(b => b.Marked("pad3"));
            app.Tap(b => b.Marked("pad0"));
            app.Tap(b => b.Marked("pad0"));
            app.Tap(b => b.Marked("Guardar"));

            //Assert
            AppResult[] results = app.WaitForElement(c => c.Marked("titleText").Text(note));
            Assert.IsTrue(results.Any());
        }
    }
}
