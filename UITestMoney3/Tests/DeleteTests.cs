﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Android;
using Xamarin.UITest.Queries;

namespace UITestMoney3
{
    [TestFixture]
    public class DeleteTests
    {
        AndroidApp app;

        [SetUp]
        public void BeforeEachTest()
        {
            //Inicialización del APK
            app = ConfigureApp
                .Android
                .ApkFile("C:/Users/keilo/source/repos/UITestMoney3/money.apk")
                .StartApp();
        }

        //Prueba de delete de un registro, primero se ingresa un registro de prueba.
        [Test]
        public void DeleteTest1()
        {
            //Arrange
            string note = "DeleteTest1";
            string description = "Medicamentos de Keilor";

            //Act
            app.Tap(b => b.Marked("cerrar"));
            app.Tap(b => b.Marked("addBtnFrame"));
            app.Tap(b => b.Marked("Efectivo"));
            app.Tap(b => b.Marked("Salud"));
            app.Tap(b => b.Marked("amountBlock"));
            app.Tap(b => b.Marked("pad6"));
            app.Tap(b => b.Marked("pad5"));
            app.Tap(b => b.Marked("pad0"));
            app.Tap(b => b.Marked("pad0"));
            app.Tap(b => b.Marked("memoBlock"));
            app.EnterText(note);
            app.Tap(b => b.Marked("description"));
            app.EnterText(description);
            app.ScrollDown();
            app.Tap(b => b.Marked("Guardar"));
            
            //Eliminar registro
            app.Tap(b => b.Marked("listHeadLayout"));
            app.Tap(b => b.Marked("deleteButton"));
            app.Tap(b => b.Marked("yesButton"));

            //Assert [AppCompatTextView] text: "No hay datos."
            AppResult[] results = app.WaitForElement(c => c.Marked("noDataImg"));
            Assert.IsTrue(results.Any());
        }

    }
}
