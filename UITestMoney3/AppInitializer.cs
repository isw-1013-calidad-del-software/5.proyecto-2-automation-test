﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace UITestMoney3
{
    public class AppInitializer
    {
        public static IApp StartApp(Platform platform)
        {
            AppCenter.Start("cd11901a-0dc4-4778-be1c-7143f7932c6c",
            typeof(Analytics), typeof(Crashes));

            if (platform == Platform.Android)
            {
                IApp app = ConfigureApp
                    .Android
                    .ApkFile("C:/Users/keilo/source/repos/UITestMoney3/money.apk")
                    .StartApp();
            }

            return ConfigureApp.iOS.StartApp();
        }
    }
}